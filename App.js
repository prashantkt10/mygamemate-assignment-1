import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import LandingScreen from './screens/LandingScreen';
import HomeScreen from './screens/HomeScreen';


export default class App extends React.Component {
  render() {
    return <AppContainer />
  }
}

const AppNavigator = createStackNavigator({
  Start: { screen: LandingScreen },
  Home: { screen: HomeScreen }
},
  { initialRouteName: "Start" }
);

const AppContainer = createAppContainer(AppNavigator);