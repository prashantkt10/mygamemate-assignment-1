import React from 'react';
import { View, Image, Text, StyleSheet, Dimensions } from 'react-native';

const win = Dimensions.get('window');

export default function App() {
    return (
        <View style={styles.container}>
            <Image source={require('../assets/pubg.jpg')} style={styles.backgroundImage} />
            <View>
                <Text>Game is loading...</Text>
            </View>
        </View>
    );
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    backgroundImage: {
        flex: 1,
        width: win.width,
        height: win.height,
        resizeMode: 'cover', // or 'stretch'
    }
});