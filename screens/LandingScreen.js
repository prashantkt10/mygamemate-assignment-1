import React from 'react';
import { Button, View, StyleSheet } from 'react-native';


export default class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Button title="Start Game" onPress={() => this.props.navigation.navigate('Home')} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});